set nocompatible
filetype plugin indent on
syntax on
set number
set cursorline
set nobackup
set tabstop=4
set expandtab
set shiftwidth=4
set ignorecase
set cindent
set autoindent
set smartindent
set wildmenu
set wildmode=longest,list,full
set scrolloff=5
set encoding=utf-8
set fileencoding=utf-8
set laststatus=2
set noshowmode
set guifont=Anonymice\ Powerline\ Nerd\ Font\ Plus\ Font\ Awesome\ Plus\ Octicons\ 12

let Powerline_symbols = 'fancy'

let g:airline#extensions#tabline#enabled = 0
let g:airline_powerline_fonts = 1
let g:airline_theme = 'lucius'
let g:airline#extensions#tabline#show_buffers = 0
let g:bufferline_echo = 0
let g:Vimphpcs_Standard='PSR1,PSR2'

if &term == "screen-256color"
  set t_Co=256
  set term=gnome-256color
  let g:inkpot_black_background = 1
  colorscheme jellybeans
"  colorscheme desert
endif

" Open and close all the three plugins on the same time 
nmap <F8>  :TrinityToggleAll<CR> 
" Open and close the Source Explorer separately 
nmap <F9>  :TrinityToggleSourceExplorer<CR> 
" Open and close the Taglist separately 
nmap <F10> :TrinityToggleTagList<CR> 
" Open and close the NERD Tree separately 
nmap <F11> :TrinityToggleNERDTree<CR>

"nmap <C-P> :bp<CR>
"nmap <C-N> :bn<CR>

let g:startify_custom_header =
  \ map(split(system('fortune vimtips | cowsay -f $(ls -1 /usr/share/cows | shuf -n1)'), '\n'), '"   ". v:val') + ['','']

imap <C-o> :set paste<CR>:exe PhpDoc()<CR>:set nopaste<CR>i
inoremap <C-P> <ESC>:call PhpDocSingle()<CR>i
nnoremap <C-P> :call PhpDocSingle()<CR>
vnoremap <C-P> :call PhpDocRange()<CR>

let twitvim_enable_python3 = 1
let twitvim_browser_cmd = 'firefox'

let g:fzf_launcher = 'urxvt -geometry 120x30 -e sh -c %s'

let g:phpcomplete_index_composer_command = 'composer.phar'

" save
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>
nnoremap <Leader>x :x<CR>
nnoremap ; :
nnoremap : ;
" copypasta
vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P
nmap <Leader>t :NERDTreeToggle<CR>
vmap <Leader>t :NERDTreeToggle<CR>
" visual
nmap <Leader><Leader> V

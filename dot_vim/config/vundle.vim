set nocompatible
filetype off

set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

Plugin 'gmarik/vundle'
"Plugin 'editorconfig/editorconfig-vim'
Plugin 'tpope/vim-fugitive'
Plugin 'mattn/emmet-vim'
Plugin 'garbas/vim-snipmate'
Plugin 'honza/vim-snippets'
Plugin 'tpope/vim-surround'
Plugin 'MarcWeber/vim-addon-mw-utils'
Plugin 'aaronbieber/vim-quicktask'
Plugin 'Dpaste.com-Plugin'
Plugin 'matchit.zip'
Plugin 'tlib'
"Plugin 'bling/vim-bufferline'
Plugin 'bling/vim-airline'
"Plugin 'Zuckonit/vim-airline-tomato'
Plugin 'scrooloose/nerdtree'
Plugin 'taglist.vim'
"Plugin 'wesleyche/SrcExpl'
"Plugin 'wesleyche/Trinity'
Plugin 'mhinz/vim-startify'
Plugin 'kchmck/vim-coffee-script'
Plugin 'PDV--phpDocumentor-for-Vim'
Plugin 'digitaltoad/vim-jade'
"Plugin 'TwitVim'
Plugin 'junegunn/fzf'
"Plugin 'DBGp-Remote-Debugger-Interface'
"Plugin 'oplatek/Conque-Shell'
"Plugin 'Keithbsmiley/investigate.vim'
"Plugin 'krisajenkins/vim-pipe'

" phpcomplete-extended req: vimproc
Plugin 'Shougo/vimproc.vim'
Plugin 'Shougo/unite.vim'
Plugin 'm2mdas/phpcomplete-extended'

Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
"Plugin 'fatih/vim-go'
Plugin 'jelera/vim-javascript-syntax'
Plugin 'heartsentwined/vim-emblem'
Plugin 'heartsentwined/vim-ember-script'

colorscheme herald
"set guifont=Inconsolata\ 12
set guifont=Terminus\ 10
set guioptions-=m
set guioptions-=T
set guioptions-=r
set incsearch
set hlsearch

au FileType pascal set makeprg=fpc\ %

func! myspacevim#before() abort

  " BEGIN wayland:
  " noremap ; :
  " noremap : ;
  " inoremap ; :
  " inoremap : ;
  " cnoremap ; :
  " cnoremap : ;
  " tnoremap ; :
  " tnoremap : ;
  " vnoremap ; :
  " vnoremap : ;
  " nnoremap ; :
  " nnoremap : ;
  " snoremap ; :
  " snoremap : ;
  " END wayland:

  let base16colorspace=256
  "let g:spacevim_colorscheme = 'base16-' . $BASE16_THEME
  let g:startify_custom_header = 'startify#fortune#cowsay()'
  let g:startify_custom_header =
    \ map(split(system('myfortune'), '\n'), '"   ". v:val')
endf

func! myspacevim#after() abort
  set ignorecase
  hi Normal guibg=NONE ctermbg=NONE
  hi EndOfBuffer guibg=NONE ctermbg=NONE
endf

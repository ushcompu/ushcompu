#!/bin/sh

########################################
# needs: imagemagick, curl, feh and bc #
########################################

# crontab it, every 10 minutes:
# */10 * * * * env DISPLAY=:0 sh scripts/background_wttr.sh $HOME/Pictures/IMG_20220209_194632.jpg "CABA, Argentina"

if [ $# -lt 2 ] ; then
  echo 'Usage: '$0' $HOME/Pictures/IMG_20220209_194632.jpg "CABA, Argentina"'
  exit 1
fi

black=`realpath ~/Downloads/Solid_black.png`
set_back() {
  # general
  feh --no-fehbg --bg-fill $1
  # xfce
  xfconf-query -c xfce4-desktop -p /backdrop/screen0/monitoreDP-1/workspace0/last-image -s $temp_comp
  # kde
  set_kde_back $black
  set_kde_back $1
  # # hyprland
  # hyprctl hyprpaper preload "${1}"
  # hyprctl hyprpaper wallpaper "eDP-1,${1}"
}

set_kde_back() {
  qdbus org.kde.plasmashell /PlasmaShell org.kde.PlasmaShell.evaluateScript '
      var allDesktops = desktops();
      print (allDesktops);
      for (i=0;i<allDesktops.length;i++) {{
          d = allDesktops[i];
          d.wallpaperPlugin = "org.kde.image";
          d.currentConfigGroup = Array("Wallpaper",
                                       "org.kde.image",
                                       "General");
          d.writeConfig("Image", "file://'$1'?f='$RANDOM'")
      }}
'
}

background=$1
set_back $1
city=`echo $2 | sed 's/ /%20/g'`

dimensions=`xdpyinfo | grep -oP 'dimensions:\s+\K\S+'`
width=`echo $dimensions | sed 's/x.*//'`
height=`echo $dimensions | sed 's/.*x//'`
inner=`echo "$width * 0.90/ 1" | bc`
text=`date`

temp_wttr=`mktemp /tmp/backgr.wttr.XXXXXXXXXX.png`
temp_comp=/tmp/backgr.wttr.$USER.jpg

url="https://wttr.in/${city}_lang=es_fmt=.png"
#curl -o $temp_wttr $url
wego | textimg -f /usr/share/fonts/liberation/LiberationMono-Regular.ttf -o $temp_wttr

composite -dissolve 50 -gravity Center \
  \( $temp_wttr -resize $inner \) \( $background -resize $width \) \
  -alpha Set $temp_comp
convert $temp_comp -gravity NorthEast -pointsize 24 -fill white -annotate +24+$height "${text}" $temp_comp

set_back $temp_comp
rm $temp_wttr

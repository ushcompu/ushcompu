#!/bin/sh

DIR="."
strip=$1
slug=$(echo $strip | tr / _)
img=$(curl http://www.gocomics.com/$1/ | grep 'mutable.*display: none' | sed -re 's/.* src="(http.*)" .*/\1/')
curl $img > $DIR/$slug.jpg

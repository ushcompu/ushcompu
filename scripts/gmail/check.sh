#!/bin/sh

datafile="$HOME/scripts/gmail/data.txt"
countfile="$HOME/scripts/gmail/count.txt"
once="$HOME/scripts/gmail/once.sh"

if [ $# -ge 1 ]; then
  email=$1
else
  echo -n "Email: "
  read email
fi
if [ $# -ge 2 ]; then
  password=$2
else
  echo -n "Password: "
  read -s password
  echo " "
fi

$once $email $password || exit 1

while [ true ]
do
  sleep 60
  $once $email $password
done &

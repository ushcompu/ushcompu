#!/bin/sh

datafile="$HOME/scripts/gmail/data.txt"
countfile="$HOME/scripts/gmail/count.txt"

if [ $# -ge 1 ]; then
  email=$1
else
  echo -n "Email: "
  read email
fi
if [ $# -ge 2 ]; then
  password=$2
else
  echo -n "Password: "
  read -s password
  echo " "
fi

wget -q --http-user=$email --http-password=$password -O $datafile https://mail.google.com/mail/feed/atom \
|| { echo "Incorrect email or password"; exit 1; }
grep fullcount $datafile | sed 's/<[a-zA-Z\/][^>]*>//g' > $countfile

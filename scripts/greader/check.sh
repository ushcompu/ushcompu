#!/bin/sh

countfile="$HOME/scripts/greader/count.txt"
datafile="$HOME/scripts/greader/data.txt"
once="$HOME/scripts/greader/once.sh"

if [ $# -ge 1 ]; then
  email=$1
else
  echo -n "Email: "
  read email
fi
if [ $# -ge 2 ]; then
  password=$2
else
  echo -n "Password: "
  read -s password
  echo " "
fi

$once $email $password || exit 1

while [ true ]
do
  sleep 120
  $once $email $password
done &

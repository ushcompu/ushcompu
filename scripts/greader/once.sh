#!/bin/sh

countfile="$HOME/scripts/greader/count.txt"
datafile="$HOME/scripts/greader/data.txt"

if [ $# -ge 1 ]; then
  email=$1
else
  echo -n "Email: "
  read email
fi
if [ $# -ge 2 ]; then
  password=$2
else
  echo -n "Password: "
  read -s password
  echo " "
fi

sid=$(curl -s -d "Email=$email&Passwd=$password&service=reader" https://www.google.com/accounts/ClientLogin | grep '^SID' | sed 's/^SID=//')

if [ -z "$sid" ]
then
  echo 'Incorrect email or password'
  exit 1
fi

ck=$(date +'%s')

curl -s -b "SID=$sid" 'https://www.google.com/reader/api/0/unread-count?all=true&output=json&ck='$ck | tr -d "\n" > $datafile
count=$(grep 'com.google/reading-list"' $datafile | sed -r 's/.*"user\/[0-9]*\/state\/com\.google\/reading-list","count":([0-9]*),.*/\1/')
if [ -z "$count" ]
then
  count=0
fi
echo $count > $countfile

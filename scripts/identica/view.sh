#!/bin/sh
#===============================================================================
#
#          FILE:  view.sh
# 
#         USAGE:  ./view.sh username
#                 ./view.sh group/groupname
# 
#   DESCRIPTION:  view messages
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  curl, xsltproc
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  totoloco (totoloco at gmail dot com), 
#       COMPANY:  
#       VERSION:  0.1.0
#       CREATED:  08/07/09 14:22:25 ART
#      REVISION:  ---
#       LICENSE:  Sisteware
#===============================================================================

#curl - s http://identi.ca/$1/rss | xsltproc rss10.xsl -
curl http://identi.ca/$1/rss | xsltproc rss10.xsl -

#!/bin/sh
#===============================================================================
#
#          FILE:  post.sh
# 
#         USAGE:  ./post.sh 'message'
# 
#   DESCRIPTION:  post to identi.ca microblog
# 
#       OPTIONS:  ---
#  REQUIREMENTS:  curl
#          BUGS:  ---
#         NOTES:  ---
#        AUTHOR:  totoloco (totoloco at gmail dot com), 
#       COMPANY:  
#       VERSION:  0.2.0
#       CREATED:  07/07/09 14:32:37 ART
#      REVISION:  07/08/09 14:24:37 ART
#       LICENSE:  Sisteware
#===============================================================================

# leave blank user & pass to get more security
user="totoloco"
pass=""

if [ "$user" = "" ] ; then
  echo -n 'User: '
  read user
fi

if [ "$pass" = "" ] ; then
  echo -n 'Pass: '
  read -s pass
fi

echo
echo "Posting..."

curl -u $user:$pass -d "status=$*" http://identi.ca/api/statuses/update.xml 

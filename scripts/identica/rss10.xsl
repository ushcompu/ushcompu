<?xml version="1.0" encoding="UTF-8"?> 
<xsl:stylesheet
 version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
 xmlns:rss="http://purl.org/rss/1.0/"
 xmlns:dc="http://purl.org/dc/elements/1.1/"
 xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
 xmlns:content="http://purl.org/rss/1.0/modules/content/"
 xmlns:admin="http://webns.net/mvcb/"
 >
  <xsl:output method="text" xdisable-output-escaping="yes"/>
  <xsl:template match="/">
    <xsl:for-each select="/rdf:RDF/rss:item">
      <!--
      <xsl:value-of select="dc:date"/>
      <xsl:text>: </xsl:text>
      -->
      <xsl:value-of select="rss:title"/>
      <xsl:text>&#xa;</xsl:text>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>

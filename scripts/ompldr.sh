#!/bin/sh
id=$(curl  -F "file1=@$1" http://ompldr.org/upload \
        | grep 'File:' \
        | sed -r 's/.*http:\/\/ompldr.org\/v([a-zA-Z0-9]*)<.*$/\1/')
base=$(basename $1)
echo http://ompldr.org/v$id
echo http://ompldr.org/v$id/$base
echo http://ompldr.org/t$id

#!/usr/bin/env node

'use strict'
const fs = require('fs')
const mget = require('miniget')

// 4795 -> Campana
// 4818 -> Escobar
const CITY = 4818
const CITY_URL = `https://ws1.smn.gob.ar/v1/weather/location/${CITY}`

main()

async function main() {
  const minutes = parseInt(process.argv[2])
  oneTime()
  if (!isNaN(minutes)) {
    setInterval(oneTime, minutes * 60 * 1000)
  }
}

async function oneTime() {
  const jwt = await getJwt()
  if (jwt == '') {
    return 'ERROR'
  }
  const weath = await getWeather(jwt)
  const dir = process.argv[1].replace('index.js', '')
  fs.writeFileSync(dir + 'weather.txt', weath)
}

async function getJwt() {
  const homepage = await mget('https://www.smn.gob.ar/').text()
  const regex = /localStorage.setItem\('token'\,\s?'(.*)'\)/
  const m = homepage.match(regex)
  let jwt = ''
  if (m && m.length > 1) {
    jwt = m[1]
  }
  return jwt
}

async function getWeather(jwt) {
  const w = await mget(CITY_URL, { headers: {
    authorization: 'JWT ' + jwt,
  }}).text()
  const data = JSON.parse(w)
  const temperature = data.temperature
  const descr = data.weather.description.toLowerCase()
  // return `${temperature}° ${descr}`
  return `${temperature}°C
${descr}
${new Date()}
`
}

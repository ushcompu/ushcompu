{EventEmitter} = require "events"
{MailParser}   = require "mailparser"
prompt   = require "prompt"
fs       = require "fs"
Imap     = require "imap"

class Notifv2 extends EventEmitter
    imapConfig:
        user: null
        password: null
        host: "imap.gmail.com"
        port: 993 # imap port
        tls: true # use secure connection
    file: "notif.data"
    mailbox: "INBOX"

    constructor: (user, password) ->
        @imapConfig.user = user if user?
        @imapConfig.password = password if password?

    start: ->
        if @imapConfig.password?
            @_init(@)
        else
            _this = @
            @_promptPass =>
                @_init(_this)

    _promptPass: (cb) ->
        $this = @
        prompt.get [{name: 'password', hidden: true}], (err, result) =>
            if err
                @emit "error", err
            @imapConfig.password = result.password
            cb()

    _init: (_this) ->
        @imap = new Imap @imapConfig
        @imap.once 'ready', (err) =>
            if err
                @emit "error", err
            else
                @emit "server:connected"
                @imap.openBox @mailbox, false, (err) =>
                    if err
                        @emit "error", err
                    else
                        @_parseUnreadEmails()
                        # 3. listen for new emails in the inbox
                        @imap.on "mail", (id) =>
                            @emit "mail:arrived", id
                            # 4. find all unseen emails
                            @_parseUnreadEmails()

    _parseUnreadEmails: =>
        fs.openSync _this.file, 'w'
        @imap.search ["UNSEEN"], (err, searchResults) =>
            if err
                @emit "error", err
            else
                if Array.isArray(searchResults) and searchResults.length == 0
                    return
                fetch = @imap.fetch(searchResults, { bodies: '', markSeen: false })
                fetch.on "message", (msg, id) =>
                    parser = new MailParser
                    parser.on "end", (mail) =>
                        mail.uid = id
                        @emit "mail:parsed", mail
                    msg.on "body", (stream, info) =>
                        buffer = '';
                        stream.on "data", (chunk) =>
                            buffer += chunk
                        stream.once "end", ->
                            parser.write buffer
                    msg.on "end", ->
                        parser.end()

module.exports = Notifv2

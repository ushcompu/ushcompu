prompt   = require("prompt")
notifier = require("mail-notifier")
fs       = require("fs")

class Notif
    imapConfig:
        user: null
        password: null
        host: "imap.gmail.com"
        port: 993 # imap port
        tls: true # use secure connection
        tlsOptions:
            rejectUnauthorized:
                false
        markSeen: false
    file: "notif.data"

    constructor: (user, password) ->
        @imapConfig.user = user if user?
        @imapConfig.password = password if password?

    start: ->
        if @imapConfig.password?
            @_init(@)
        else
            _this = @
            @_promptPass =>
                @_init(_this)

    _promptPass: (cb) ->
        $this = @
        prompt.get [{name: 'password', hidden: true}], (err, result) =>
            if err
                throw err
            @imapConfig.password = result.password
            do cb

    _init: (_this) ->
        fs.openSync _this.file, 'w'
        _notif = notifier(_this.imapConfig).on 'mail', (mail) =>
            out =
                from:
                    mail.from
                subject:
                    mail.subject
            fs.writeFileSync _this.file, JSON.stringify(out)+"\n"
        do _notif.start

module.exports = Notif

#!/bin/sh
# creator: matias@russitto.com
if [ $# -ne 3 ] ; then
    echo "Usage: $0 [laravel_framework_dir] [public_destination_dir] [private_destination_dir]"
fi

mkdir -p $2 $3

from=$(realpath $1)
public=$(realpath $2)
private=$(realpath $3)

echo "Copying public directory..."
cp -R $from/public/* $from/public/.htaccess $public
echo "Copying private directory..."
cp -R $from/app/ $from/artisan $from/bootstrap/ $from/composer.json $from/composer.lock $from/CONTRIBUTING.md $from/phpunit.xml $from/readme.md $from/server.php $private

priv=$(echo $private | sed 's/\//\\\//g')
fr=$(echo $from | sed 's/\//\\\//g')
pub=$(echo $public | sed 's/\//\\\//g')

echo "Patching public index..."
sed -i "s/__DIR__\.'\/\.\./'${priv}/" $public/index.php
echo "Patching bootstrap autoload..."
sed -i "s/__DIR__\.'\/\.\./'${fr}/" $private/bootstrap/autoload.php
echo "Patching bootstrap paths..."
sed -i "s/__DIR__\.'\/\.\.\/public/'${pub}/" $private/bootstrap/paths.php

echo "Giving execute permissions to artisan..."
chmod u+x $private/artisan
echo "Linking public to $private/public..."
ln -s $public $private/public

echo "
DONE!
Public  dir: ${public}
Private dir: ${private}"

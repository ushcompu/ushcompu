#!/usr/bin/python2
# TODO migrar a py3
# puro scripting, ni funciones ni OOP
import imaplib,sys,time,getpass,os

fil='/home/matias/scripts/imap/data.txt'
def_server='imap.server.com'
def_user='username@server.com'

print '[',def_server,']'
server=raw_input('Server: ')
if server=='':
  server=def_server

print '[',def_user,']'
user=raw_input('Email / User: ')
if user=='':
   user=def_user

#password=sys.argv[3]
password=getpass.getpass('Password: ')

try:
  pid = os.fork()
except OSError, e:
  raise Exception, "%s [%d]" % (e.strerror, e.errno)

if (pid == 0):	# The first child.
  while True:
    imap=imaplib.IMAP4_SSL(server)
    
    try:
      imap.login(user, password)
    except:
      print 'user / pass error'
      sys.exit(1)
    
    imap.select('Inbox')
    status, data=imap.search(None, 'UNSEEN')
    nums=data[0].split()
    count=len(nums)
    if (count > 0):
      os.system('email-alert');
    fh=os.open(fil, os.O_WRONLY | os.O_NONBLOCK | os.O_CREAT)
    os.write(fh, str(count))
    os.close(fh)

    print count
    time.sleep(60)
else:
  print 'Running as daemon :>'
  os._exit(0)	# Exit parent of the first child.

#!/usr/bin/python
# TODO pasar todo a clases
# por ahora solo trabaja con imap en ssl
import imaplib, sys, time, getpass, os

file = '/home/totoloco/scripts/imap/data.txt'
server = sys.argv[1]
user = sys.argv[2]
password = sys.argv[3]

imap = imaplib.IMAP4_SSL(server)

try:
  imap.login(user, password)
except:
  print 'user / pass error'
  sys.exit(1)

imap.select('Inbox')
status, data = imap.search(None, 'UNSEEN')
nums = data[0].split()
count = len(nums)
fh = os.open(file, os.O_WRONLY | os.O_NONBLOCK | os.O_CREAT)
os.write(fh, str(count))
os.close(fh)

#!/bin/sh
once="$HOME/scripts/imap/once.py"

DEF_SERVER='imap.server.com'
DEF_EMAIL='name@server.com'

if [ $# -ge 1 ]; then
  server=$1
else
  echo -n "Server [$DEF_SERVER]: "
  read server
  [ -z $server ] && server=$DEF_SERVER
fi
if [ $# -ge 2 ]; then
  email=$2
else
  echo -n "Email [$DEF_EMAIL]: "
  read email
  [ -z $email ] && email=$DEF_EMAIL
fi
if [ $# -ge 3 ]; then
  password=$3
else
  echo -n "Password: "
  read -s password
  echo " "
fi

$once $server $email $password || exit 1

while [ true ]
do
  sleep 60
  $once $server $email $password
done &

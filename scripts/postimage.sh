#!/bin/sh
if [ "$#" -lt 1 ]
then
  echo File???
  exit 2
fi

# sed para quedarme sólo con la url
url=$(curl -F "upload[]=@"$1 http://postimage.org/ | sed -r "s/.*(http.*)'.*/\1/")
echo "Info: " $url
# sed para intercambiar el código de seguridad por full
full=$(echo $url | sed -r "s/\/[a-z0-9]*\/$/\/full/")
echo $full

#!/usr/bin/env node

const DEF_NOTES_MIN = 1
const DEF_NOTES_MAX = 7
const DEF_LEFT_OCTAVE_IDX = 1.4
const DEF_BEM_5 = 1.2
const DEF_CHORD_INVERSION = false
const NOTES = [
  'C ',
  'C#',
  'D ',
  'D#',
  'E ',
  'F ',
  'F#',
  'G ',
  'G#',
  'A ',
  'A#',
  'B ',
]
const CHORDS = [
  'maj',
  'maj',
  'min',
  'min',
  'min',
  'aug',
  'dim',
  'sus2',
  'sus4',
]
const CHORD_EXTRAS = [
  '',
  '',
  '',
  ' min7',
  ' maj7',
  ' dim7',
  ' oct',
]
// chord inversion count = chord fingers - 1

main()

function main() {
  const len = rand(0, 7)
  for (let i = 0; i < len; i++) {
    prn(chord())
  }
}

function chord() {
  let note = NOTES[rand(0, 11)]
  let base = CHORDS[rand(0, CHORDS.length - 1)]
  let bemol = bem5()
  let extra = CHORD_EXTRAS[rand(0, CHORD_EXTRAS.length - 1)]
  let leftOctave = Math.floor(Math.random() * DEF_LEFT_OCTAVE_IDX) > 0? ' left octave': ''
  let inv = getInversion(extra)
  return `${note} ${base}${bemol}${extra}${leftOctave}${inv}`
}

function bem5() {
  let bemol = Math.floor(Math.random() * DEF_BEM_5) > 0? ' bem5': ''
  return bemol
}

function getInversion(extra) {
  let len = 3
  if (extra != '') len++
  if (rand(1, 2) == 1) return ''
  let inv = rand(1, len - 1)
  return ` ${inv}th inversion`
}

function rand(min, max) {
  const left = max - min + 1
  return Math.floor(Math.random() * (left)) + min
}

function prn(obj) {
  console.log(obj)
}

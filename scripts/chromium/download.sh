#!/bin/sh
#        AUTHOR:  totoloco at gmail.com :: www.ushcompu.com.ar
#       LICENSE:  Sisterware

latest=$(wget -qO- http://build.chromium.org/buildbot/snapshots/chromium-rel-linux/LATEST)
wget -c -O chrome-linux-$latest.zip http://build.chromium.org/buildbot/snapshots/chromium-rel-linux/$latest/chrome-linux.zip

# Optional: unpack
rm -rf chrome-linux
unzip chrome-linux-$latest.zip
# Optional: replaces library references in chrome binary file
sed -i 's,libnss3.so.1d,libnss3.so\x00\x00\x00,g;
           s,libnssutil3.so.1d,libnssutil3.so\x00\x00\x00,g;
           s,libsmime3.so.1d,libsmime3.so\x00\x00\x00,g;
           s,libssl3.so.1d,libssl3.so\x00\x00\x00,g;
           s,libplds4.so.0d,libplds4.so\x00\x00\x00,g;
           s,libplc4.so.0d,libplc4.so\x00\x00\x00,g;
           s,libnspr4.so.0d,libnspr4.so\x00\x00\x00,g;' chrome-linux/chrome
# Optional link mozilla plugins to chrome
ln -s /usr/lib/mozilla/plugins chrome-linux/plugins

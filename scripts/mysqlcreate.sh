#!/bin/sh
echo create database "${@};"
echo grant usage on "*.*" to ${@}@localhost identified by "'${@}';"
echo grant all privileges on ${@}.* to ${@}@localhost";"

#!/bin/sh
# Usage:
#   ./wallbase.sh minimal dark
#
# Recommendation:
#   Locate it in ~/.wallbase/ dir

urlbase="http://wallbase.cc/search"
postparams="orderby=random&query=$*"

files="$HOME/.wallbase/randoms.html"
file="$HOME/.wallbase/random.html"
img="$HOME/.wallbase/wall.img"

# downloading image list html
wget -O $files --post-data "$postparams" $urlbase
rand=$(grep 'http://wallbase.cc/wallpaper/' $files | head -n 1 | sed -r 's/^\s+<a href=\"(http:\/\/wallbase\.cc\/wallpaper\/[0-9]+)".*/\1/')
# downloading first image html
wget -O $file $rand
rand=$(grep -A 2 bigwall $file | tail -n 1 | sed -r 's/^\s+<img src=\"(http:\/\/.*)" alt=.*$/\1/')
# downloading image
wget -O $img $rand

# optionals:
feh --bg-fill $img
rm $files $file

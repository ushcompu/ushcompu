" BEGIN base
filetype plugin indent on
syntax on
set number
set cursorline
set nobackup
set tabstop=2
set expandtab
set shiftwidth=2
set ignorecase
set cindent
set autoindent
set smartindent
set wildmenu
set wildmode=longest,list,full
set scrolloff=5
"set encoding=utf-8
set fileencoding=utf-8
set laststatus=2
set noshowmode
set splitright
set splitbelow
set relativenumber
set guifont=DejaVu\ Sans\ Mono\ for\ Powerline\ Plus\ Nerd\ File\ Types\ Mono\ Plus\ Pomicons\ 11
" END base

let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1
let Powerline_symbols = 'fancy'
let g:airline#extensions#tabline#enabled = 0
let g:airline_powerline_fonts = 1
let g:airline_theme = 'lucius'
let g:airline#extensions#tabline#show_buffers = 0
let g:bufferline_echo = 0

" colorscheme jellybeans
" colorscheme lanox
let base16colorspace=256
colorscheme base16-harmonic16-dark
let g:startify_custom_header = map(split(system('vimcow.sh'), '\n'), '"   ". v:val') + ['','']

" save
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>
nnoremap <Leader>x :x<CR>
"nnoremap ; :
"nnoremap : ;

if has('nvim')
  tnoremap <A-h> <C-\><C-n><C-w>h
  tnoremap <A-j> <C-\><C-n><C-w>j
  tnoremap <A-k> <C-\><C-n><C-w>k
  tnoremap <A-l> <C-\><C-n><C-w>l
endif

nnoremap <A-h> <C-w>h
nnoremap <A-j> <C-w>j
nnoremap <A-k> <C-w>k
nnoremap <A-l> <C-w>l

" autocomplete
let g:deoplete#enable_at_startup = 1

smap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <expr><TAB> neosnippet#expandable_or_jumpable() ? "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" BEGIN neomake
let g:neomake_open_list = 2
let g:neomake_javascript_enabled_makers = ['eslint']
" END neomake

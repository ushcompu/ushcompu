filetype off

call plug#begin('~/.config/nvim/plugged')
" BEGIN colors
Plug 'junegunn/seoul256.vim'
Plug 'notpratheek/vim-luna'
Plug 'lanox/lanox-vim-theme'
Plug 'nanotech/jellybeans.vim'
Plug 'vim-scripts/Colour-Sampler-Pack'
Plug 'chriskempson/base16-vim'
" END colors
" .editorconfig 
Plug 'editorconfig/editorconfig-vim'
" git
Plug 'tpope/vim-fugitive'
Plug 'junegunn/gv.vim'
Plug 'mattn/emmet-vim'
Plug 'tpope/vim-surround'
Plug 'Dpaste.com-Plugin'
Plug 'matchit.zip'
Plug 'tlib'
Plug 'scrooloose/nerdtree'
Plug 'mhinz/vim-startify'
Plug 'darthmall/vim-vue'
" BEGIN neovim plugs
if has('nvim')
  Plug 'Shougo/deoplete.nvim'
else
  Plug 'Shougo/neocomplete.vim'
endif
"Plug 'Shougo/neosnippet'
"Plug 'Shougo/neosnippet-snippets'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
" END neovim plugs
Plug 'freitass/todo.txt-vim'
" BEGIN airline
Plug 'bling/vim-airline'
Plug 'ryanoasis/vim-devicons'
Plug 'ryanoasis/nerd-fonts'
Plug 'vim-airline/vim-airline-themes'
" END airline
Plug 'Glench/Vim-Jinja2-Syntax'
"Plug 'scrooloose/syntastic'
Plug 'neomake/neomake'

call plug#end()

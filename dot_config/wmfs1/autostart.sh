#!/bin/sh
~/bin/mylatam
~/bin/myredshift &
xscreensaver -no-splash &
xbindkeys
urxvt -e ~/bin/mitmux &
source ~/.fehbg
#while true; do $XDG_CONFIG_HOME/wmfs/status.sh; sleep 2; done &
while (ps ax | grep '[w]mfs$'); do $XDG_CONFIG_HOME/wmfs/status.sh; sleep 2; done
killall redshift
killall autostart.sh

// totoloco en gmx
// MIT License
// 20100128
// based on input_inting_mode.js from vimprobable2
(function (window, undefined) {
  main = function (byDom) {
    with(KeyEvent) {
      add(['m']  , Hint.start         );
      add(['M']  , Hint.new_tab_start );
      add(['Esc'], Hint.remove        );
      add(['Esc'], Hint.remove , true );
    }
    with(CmdLine) {
      add('bdelete', Buffer.deleteMatchHandle );
    }
    KeyEvent.init();
    console.log('DOMContentLoaded');
  };

  DOMContentLoaded = function () {
    document.removeEventListener('DOMContentLoaded', DOMContentLoaded, false);
    main(true);
  };

  if (typeof(retumiloDomIsLoaded) == 'undefined')
    document.addEventListener('DOMContentLoaded', DOMContentLoaded, false);
  window.retumiloDomIsLoaded = true;
})(window);

// source from vrome
var Hint = (function() {
  var elements    = [];
  var numbers     = 0;
  var currentHint = false;
  var new_tab     = false;
  var matched     = [];
  var hintMode    = false;
  var highlight   = 'vrome_highlight';

  function start(newTab){
    console.log('start hinting...');
    hintMode    = true;
    elements    = [];
    numbers     = 0;
    currentHint = false;
    new_tab     = newTab;
    setHints();
    CmdBox.set({title : 'HintMode',pressDown : handleInput,content : ''});
  }

  function setHints() {
    var elems = document.body.querySelectorAll('a, input:not([type=hidden]), textarea, select, button,*[onclick]');
    for (var i = 0; i < elems.length; i++) {
      if (isElementVisible(elems[i])){
        elements.push(elems[i]);
      }
    }
    setOrder(elements);
    matched = elements;
  }

  function setOrder(elems){
    // delete old highlight hints
    for (var i = 0; i < elements.length; i++) {
      elements[i].removeAttribute(highlight);
    }

    var div = document.getElementById('__vim_hint_highlight');
    if(div) document.body.removeChild(div);
    div = document.createElement('div');
    div.setAttribute('id', '__vim_hint_highlight');
    document.body.appendChild(div);

    for(var i = 0;i < elems.length; i++){ //TODO need refactor
      elem          = elems[i];
      var win_top   = window.scrollY / Zoom.current();
      var win_left  = window.scrollX / Zoom.current();
      var pos = elem.getBoundingClientRect();
      var elem_top  = win_top + pos.top;
      var elem_left = win_left + pos.left;

      var span = document.createElement('span');
      span.setAttribute('id', '__vim_hint_highlight_span');
      span.style.left            = elem_left + 'px';
      span.style.top             = elem_top  + 'px';
      span.style.backgroundColor = 'red';
      span.innerHTML             = Number(i) + 1; // cur
      div.appendChild(span);

      setHighlight(elem, false);
    }
    if (elems[0] && elems[0].tagName == 'A') setHighlight(elems[0], true);
  }

  function setHighlight(elem, is_active) {
    if(!elem) { return false; }

    if (is_active) {
      var active_elem = document.body.querySelector('a[' + highlight + '=hint_active]');
      if (active_elem){
        active_elem.setAttribute(highlight, 'hint_elem');
      }
      elem.setAttribute(highlight, 'hint_active');
    } else {
      elem.setAttribute(highlight, 'hint_elem');
    }
  }

  function remove(){
    if(!hintMode) return;
    CmdBox.remove();
    hintMode = false;

    for (var i = 0; i < elements.length; i++) {
      elements[i].removeAttribute(highlight);
    }

    var div = document.getElementById('__vim_hint_highlight');
    if (div) { document.body.removeChild(div); }
  }

  function handleInput(e){
    key = getKey(e);

    if(/^\d$/.test(key) || (key == 'BackSpace' && numbers != 0)){
      numbers = (key == 'BackSpace') ? parseInt(numbers / 10) : numbers * 10 + Number(key);
      CmdBox.set({title : 'HintMode (' + numbers + ')'});
      var cur = numbers - 1;

      setHighlight(matched[cur],true);
      currentHint = matched[cur];
      e.preventDefault();

      if (numbers * 10 > matched.length){
        return execSelect( currentHint );
      }
    }else{
      CmdBox.set({title : 'HintMode'});
      if(key != 'Esc') setTimeout(delayToWaitKeyDown,50);
    }
  }

  function delayToWaitKeyDown(){
    numbers = 0;
    matched = [];

    for(var i in elements){
      if ( new RegExp(CmdBox.get().content,'im').test(elements[i].innerText) ){
        matched.push(elements[i]);
      }
    }

    setOrder(matched);

    if (key == 'Enter' || matched.length == 1) {
      return execSelect(currentHint ? currentHint : matched[0]);
    }
    currentHint = false;
  }

  function execSelect(elem) {
    if(!elem){ return false; }
    var tag_name = elem.tagName.toLowerCase();
    var type     = elem.type ? elem.type.toLowerCase() : "";

    if (tag_name == 'a') {
      setHighlight(elem, true);
      if(!new_tab){
        var old_target = elem.getAttribute('target');
        elem.removeAttribute('target');
      }

      clickElement(elem,{ ctrl : new_tab });
      if (old_target) elem.setAttribute('target',old_target);

    } else if (tag_name == "input" && (type == "submit" || type == "button" || type == "reset" || type == "radio" || type == "checkbox")) {
      clickElement(elem);

    } else if (tag_name == 'input' || tag_name == 'textarea') {
      try{
        elem.focus();
        elem.setSelectionRange(elem.value.length, elem.value.length);
      }catch(e){
        clickElement(elem); // some website don't use standard submit input.
      }

    } else if (tag_name == 'select'){
      elem.focus();

    } else if (elem.onclick) {
      clickElement(elem);
    }

    remove();
  }

  return {
    start         : start,
    new_tab_start : function(){ start(true); },
    remove        : remove
  };
})();

var KeyEvent = (function() {
  var times = 0;
  var disableVrome,pass_next_key,last_current_keys,last_times,disable_site;

  function init() {
    document.addEventListener('keydown',KeyEvent.exec, false);
  }

  function returnTimes(/*Boolean*/ read) {
    var old_times = times;
    if(!read) times = 0; // only read,don't reset it
    return old_times;
  }

  ///////////////////////////////////////////////////
  // Last Commands
  ///////////////////////////////////////////////////
  function setLast(/*Array*/ currentKeys,/*Number*/ times){
    times = times || 0;
    Post({action : "setLastCommand",currentKey : currentKeys,times : times });
    last_current_keys = currentKeys;
    last_times        = times;
    Debug("KeyEvent.setLast - currentKeys:" + currentKeys + " times:" + times);
  }

  function runLast(){
    runCurrentKeys(last_current_keys);
  }

  ///////////////////////////////////////////////////
  var bindings    = [];
  var currentKeys = [];

  function add(/*Array*/ keys,/*Function*/ fun,/*Boolean*/ input){
    if(typeof keys == 'string'){ keys = Array(keys); }
    bindings.push([keys,fun,!!input]);
  }

  function reset(){
    currentKeys = [];
  }

  ///////////////////////////////////////////////////
  function passNextKey(){
    CmdBox.set({title : ' -- PASS THROUGH (next) -- ',timeout : 2000 });
    pass_next_key  = true;
    Post({action : "Vrome.disable"})
  }

  function disable(){
    Debug("KeyEvent.disable");
    CmdBox.set({title : ' -- PASS THROUGH -- ' });
    disableVrome = true;
    Post({action : "Vrome.disable"})
  }

  function enable() {
    Debug("KeyEvent.enable");
    CmdBox.remove();
    disableVrome = false;
    pass_next_key  = false;
    reset();
    Post({action : "Vrome.enable"})
  }

  function changeStatus(/*Boolean*/ enableStatus){
    if(typeof enableStatus == "boolean") {
      disableVrome = !enableStatus;
      disable_site = disableVrome;

    }else if (typeof disableVrome == "undefined") {
      var disable_sites = Settings.get('background.disableSites');

      for(var i in disable_sites){
        if(disable_sites[i] && disable_sites[i]){
          if(new RegExp(disable_sites[i],'i').test(location.href)){
            disableVrome = true;
            disable_site = true;
            break;
          }
        }
      }
    }

    disableVrome ? disable() : enable();
  }

  ///////////////////////////////////////////////////
  function runCurrentKeys(keys, insertMode, key,e) {
    // run last command
    if(key == '.' && !insertMode){
      var old_times = last_times;
      times = (last_times || 1) * (times || 1);
    // some key pressed
    }else if(key && !insertMode){
      var old_times = times;
    }

    var matched = [];

    binding : for(var i in bindings){
      // insertMode or not
      if(!!insertMode != bindings[i][2]) continue binding;

      // part matched bindings.
      for(var j in keys){
        if(keys[j] != bindings[i][0][j]) continue binding;
      }
      matched.push(bindings[i]);
    }

    var exec_length = 0;
    for(var i in matched){
      // execute those exactly matched bindings
      if(matched[i][0].length == keys.length){
        matched[i][1].call(e);
        exec_length++;
      }
    }

    Debug("KeyEvent.runCurrentKeys - keys:" + keys + " insertMode:" + insertMode + " times:" + old_times + " matched:" + matched.length + " exec:" + exec_length);

    // store current command
    if(exec_length > 0 && key != '.' && !insertMode) setLast(keys,old_times);

    // if currentMode is not insertMode,and the key is a number,update times.
    if (!insertMode && /\d/.test(key)){
      times = (times || 0) * 10 + Number(key);
    }else{
      // if some function executed and some key pressed, reset the times
      // no key perssed always means,this function is invoked by runLastCommand.
      if(exec_length != 0 && key) times = 0;
    }

    // reset if all matched bindings has been executed,or the key is Esc,or no key
    if(matched.length == exec_length || key == 'Esc' || !key){ reset(); }

    // if any command executed,and the key is not Enter in insertMode (submit form)
    return (exec_length > 0 && !(key == 'Enter' && insertMode));
  }

  function exec(e) {
    if(disable_site) return; // if this url belong to disabled sites,do nothing
    var key        = getKey(e);
    var insertMode = /^INPUT|TEXTAREA$/i.test(e.target.nodeName);
    if(/^(Control|Alt|Shift)$/.test(key)) return;
    currentKeys.push(key);

    // if vrome set disabled/pass the next, use Esc to enable it again.
    if ((pass_next_key || disableVrome) && !insertMode) {
      if (pass_next_key || key == 'Esc') { enable(); }
      return;
    }

    if (runCurrentKeys(currentKeys,insertMode,key,e)) e.preventDefault();
  }

  return {
    add     : add,
    exec    : exec,

    init    : init,
    times   : function(/*Boolean*/ read){ return returnTimes(read) },

    disable : disable,
    passNextKey    : passNextKey,
    changeStatus   : changeStatus,

    setLast : setLast,
    runLast : runLast,
  };
})();

var CmdBox = (function(){
  var box_id        = '_vrome_cmd_box';
  var input_box_id  = '_vrome_cmd_input_box';

  var pressUpFunction   = function(){};
  var pressDownFunction = function(){};
  var pressUp           = function(e) { pressUpFunction.call('',e);  };
  var pressDown         = function(e) { pressDownFunction.call('',e);};

  function cmdBox() {
    var div = document.getElementById(box_id);
    if(!div){
      div = document.createElement('div');
      div.setAttribute('id',box_id);
      document.body.appendChild(div);
    }
    return div;
  }

  function cmdBoxTitle() {
    return document.querySelector('#_vrome_cmd_box span');
  }

  function createCmdBoxTitle() {
    var cmdbox = cmdBox();
    var span = document.createElement('span');
    cmdbox.appendChild(span);
    return span;
  }

  function cmdBoxInput() {
    return document.querySelector('#_vrome_cmd_box input');
  }

  function createCmdBoxInput() {
    var cmdbox = cmdBox();
    var input = document.createElement('input');
    input.setAttribute('id',input_box_id);
    cmdbox.appendChild(input);
    return input;
  }

  function set(opt) {
    if(opt.title) {
      var title = cmdBoxTitle() || createCmdBoxTitle();
      title.innerText = opt.title;
    }
    if(typeof(opt.content) == 'string') {
      var input = cmdBoxInput() || createCmdBoxInput();
      input.value = opt.content;
      input.focus();
      input.setSelectionRange(0,input.value.length);
      input.addEventListener('keydown',pressDown,false);
      input.addEventListener('keyup'  ,pressUp,false);
    }
    if(opt.pressUp)
      pressUpFunction = opt.pressUp;
    if(opt.pressDown)
      pressDownFunction = opt.pressDown;
    if(opt.timeout)
      setTimeout(remove,Number(opt.timeout));
  }

  function get() {
    return {
      title   : cmdBoxTitle() ? cmdBoxTitle().innerText : '',
      content : cmdBoxInput() ? cmdBoxInput().value     : '',
    };
  }

  function remove() {
    pressUpFunction   = function(){};
    pressDownFunction = function(){};
    var box = document.getElementById(box_id);
    if(box) document.body.removeChild(box);
  }

  return { set : set, get : get, remove : remove };
})()

var CmdLine = (function() {
  var commands = [];

  function add(/*String*/ command,/*Function*/ fun){
    commands.push([command,fun]);
  }

  function start() {
    CmdBox.set({ content : ''});
  }

  function exec() {
    /^(\S+)\s+(.*)$/.test(CmdBox.get().content);
    var cmd     = RegExp.$1;
    var arg     = RegExp.$2;
    var matched = [];

    for(var i = 0; i < commands.length; i++) {
      if (new RegExp('^' + cmd).test(commands[i][0])) {
        if ( cmd == commands[i][0]) return commands[i][1].call('',arg);
        matched.push(commands[i][1]);
      }
    }
    if( matched.length == 1 ) return matched[0].call('',arg);
  }

  return { start : start, exec : exec , add : add };
})()

var Buffer = (function(){
  var bufferGotoMode,bufferMatchMode;

  function gotoFirstMatchHandle() {
    if(!bufferGotoMode) return;
    Post({ action : 'Buffer.gotoFirstMatch', keyword : CmdBox.get().content });
    bufferGotoMode = false;
    CmdBox.remove();
  }

  function gotoFirstMatch() {
    var count = times(/*raw*/ true);

    if(count){
      Post({action: "Tab.goto",index : count - 1});
    }else{
      bufferGotoMode = true;
      CmdBox.set({ title   : 'Buffer ', content : '' });
    }
  }

  // keyword for CmdLine
  function deleteMatchHandle(keyword) {
    if(!keyword && !bufferMatchMode) return;
    Post({ action : 'Buffer.deleteMatch', keyword : keyword || CmdBox.get().content });
    bufferMatchMode = false;
    CmdBox.remove();
  }

  function deleteMatch() {
    bufferMatchMode = true;
    CmdBox.set({ title   : 'Delete Buffer ', content : '' });
  }

  return {
    gotoFirstMatch       : gotoFirstMatch       ,
    gotoFirstMatchHandle : gotoFirstMatchHandle ,
    deleteMatch          : deleteMatch          ,
    deleteMatchHandle    : deleteMatchHandle    ,
  }
})()

var getKey = (function(){
  var keyId = {
    "U+0008" : "BackSpace",
    "U+0009" : "Tab",
    "U+0018" : "Cancel",
    "U+001B" : "Esc",
    "U+0020" : "Space",
    "U+0021" : "!",
    "U+0022" : "\"",
    "U+0023" : "#",
    "U+0024" : "$",
    "U+0026" : "&",
    "U+0027" : "'",
    "U+0028" : "(",
    "U+0029" : ")",
    "U+002A" : "*",
    "U+002B" : "+",
    "U+00BB" : "+",
    "U+002C" : ",",
    "U+00BC" : ",",
    "U+002D" : "-",
    "U+00BD" : "-",
    "U+002E" : ".",
    "U+00BE" : ".",
    "U+002F" : "/",
    "U+00BF" : "/",
    "U+0030" : "0",
    "U+0031" : "1",
    "U+0032" : "2",
    "U+0033" : "3",
    "U+0034" : "4",
    "U+0035" : "5",
    "U+0036" : "6",
    "U+0037" : "7",
    "U+0038" : "8",
    "U+0039" : "9",
    "U+003A" : ":",
    "U+00BA" : ":",
    "U+003B" : ";",
    "U+003C" : "<",
    "U+003D" : "=",
    "U+003E" : ">",
    "U+003F" : "?",
    "U+0040" : "@",
    "U+0041" : "a",
    "U+0042" : "b",
    "U+0043" : "c",
    "U+0044" : "d",
    "U+0045" : "e",
    "U+0046" : "f",
    "U+0047" : "g",
    "U+0048" : "h",
    "U+0049" : "i",
    "U+004A" : "j",
    "U+004B" : "k",
    "U+004C" : "l",
    "U+004D" : "m",
    "U+004E" : "n",
    "U+004F" : "o",
    "U+0050" : "p",
    "U+0051" : "q",
    "U+0052" : "r",
    "U+0053" : "s",
    "U+0054" : "t",
    "U+0055" : "u",
    "U+0056" : "v",
    "U+0057" : "w",
    "U+0058" : "x",
    "U+0059" : "y",
    "U+005A" : "z",
    "U+005B" : "[",
    "U+005C" : "\\",
    "U+005D" : "]",
    "U+00DB" : "[",
    "U+00DC" : "\\",
    "U+00DD" : "]",
    "U+005E" : "^",
    "U+005F" : "_",
    "U+0060" : "`",
    "U+007B" : "{",
    "U+007C" : "|",
    "U+007D" : "}",
    "U+007F" : "Delete",
    "U+00A1" : "¡",
    "U+0300" : "CombAcute",
    "U+0302" : "CombCircum",
    "U+0303" : "CombTilde",
    "U+0304" : "CombMacron",
    "U+0306" : "CombBreve",
    "U+0307" : "CombDot",
    "U+0308" : "CombDiaer",
    "U+030A" : "CombRing",
    "U+030B" : "CombDblAcute",
    "U+030C" : "CombCaron",
    "U+0327" : "CombCedilla",
    "U+0328" : "CombOgonek",
    "U+0345" : "CombYpogeg",
    "U+20AC" : "€",
    "U+3099" : "CombVoice",
    "U+309A" : "CombSVoice",
    "U+00C0" : "`",
  };

  var shiftNums = { "`":"~",
    "1":"!", "2":"@", "3":"#", "4":"$", "5":"%", "6":"^", "7":"&", "8":"*", "9":"(", "0":")",
    "-":"_", "=":"+", ";":":", "'":"\"", ",":"<", ".":">",  "/":"?",  "\\":"|" };

  function getKey(evt){
    var key = keyId[evt.keyIdentifier] || evt.keyIdentifier,
    ctrl = evt.ctrlKey ? 'C-' : '',
    meta = (evt.metaKey || evt.altKey) ? 'M-' : '',
    shift = evt.shiftKey ? 'S-' : '';

    if (evt.shiftKey){
      if (/^[a-z]$/.test(key)){
        return ctrl+meta+key.toUpperCase();
      }
      if (shiftNums[key]){
        return ctrl+meta+shiftNums[key];
      }
      if (/^[0-9]$/.test(key)) {
        if(key == "4") key = "$";
        return key;
      }
      if (/^(Enter|Space|BackSpace|Tab|Esc|Home|End|Left|Right|Up|Down|PageUp|PageDown|F(\d\d?))$/.test(key)){
        return ctrl+meta+shift+key;
      }
    }
    return ctrl+meta+key;
  }

  return getKey;
})()

var Zoom = (function(){
  var levels = ['30%', '50%', '67%', '80%', '90%', '100%', '110%', '120%', '133%', '150%', '170%', '200%', '240%', '300%'];
  var default_index = levels.indexOf('100%');

  function currentLevel() {
    for(var i in levels){
      if(levels[i] == (document.body.style.zoom || '100%')){
        return Number(i);
      }
    }
  }

  function setZoom(/*Number*/ count,/*Boolean*/ keepCurrentPage) {
    var index = count ? (currentLevel() + (times() * Number(count))) : default_index
    // index should >= 0 && < levels.length
    index = Math.min(levels.length - 1, Math.max(0,index));

    Settings.add({zoom_level : index - default_index});
    var topPercent = scrollY / document.height;

    document.body.style.zoom  = levels[index];
    if(keepCurrentPage) scrollTo(0,topPercent * document.height);
  }


  return {
    setZoom    : setZoom,
    'in'       : function() { setZoom( 1); },
    out        : function() { setZoom(-1); },
    more       : function() { setZoom( 3); },
    reduce     : function() { setZoom(-3); },
    reset      : function() { setZoom(  ); },

    cur_in     : function() { setZoom( 1, true); },
    cur_out    : function() { setZoom(-1, true); },
    cur_more   : function() { setZoom( 3, true); },
    cur_reduce : function() { setZoom(-3, true); },
    cur_reset  : function() { setZoom( 0, true); },

    current    : function() { return (parseInt(levels[currentLevel()]) / 100); },
    init       : function() { Zoom.setZoom( Settings.get('zoom_level')); }
  }
})()

var Debug = (function(){
  return function(str) {
    Post({action : 'debug' , message : str});
  };
})();

// UTILS
var times = function(/*Boolean*/ raw,/*Boolean*/ read) {
  var count = raw ? KeyEvent.times(read) : (KeyEvent.times(read) || 1);
  Debug('KeyEvent.times:' + count);
  return count;
};

var Post = function(msg){
  /*
  var port = chrome.extension.connect();
  port.postMessage(msg);
  */
}

function isElementVisible(elem) {
  var win_top     = window.scrollY / Zoom.current();
  var win_bottom  = win_top + window.innerHeight;
  var win_left    = window.scrollX / Zoom.current();
  var win_right   = win_left + window.innerWidth;

  var pos = elem.GETbOUNDIngClientRect();
  var elem_top    = win_top  + pos.top;
  var elem_bottom = win_top  + pos.bottom;
  var elem_left   = win_left + pos.left;
  var elem_right  = win_left + pos.left;

  return pos.height != 0 && pos.width != 0 && elem_bottom >= win_top && elem_top <= win_bottom && elem_left <= win_right && elem_right >= win_left;
}

function clickElement(element,opt) {
  //event.initMouseEvent(type, canBubble, cancelable, view,
  //                     detail, screenX, screenY, clientX, clientY,
  //                     ctrlKey, altKey, shiftKey, metaKey,
  //                     button, relatedTarget);
  // https://developer.mozilla.org/en/DOM/event.initMouseEvent
  opt = opt || {};

  var event = document.createEvent("MouseEvents");
  event.initMouseEvent("click", true, true, window,
      0, 0, 0, 0, 0,
      !!opt.ctrl, !!opt.alt, !!opt.shift, !!opt.meta,
      0, null);
  element.dispatchEvent(event);
}

function runIt(func,args){
  if(func) initFunction.push([func,args]);

  if(document.body){
    for(var i = 0;i < initFunction.length; i++){
      func = initFunction[i];
      if(func instanceof Function){
        Debug("RunIt:" + func);
        func.call();
      }else{
        if(func[0] instanceof Function){
          Debug("RunIt: function" + func[0] + " arguments:" + func[1]);
          func[0].apply('',func[1]);
        }
      }
    }
    initFunction = [];
  }else{
    setTimeout(runIt,50);
  }
}

function getSelected(){
  var value  = window.getSelection().focusNode.data;
  if(!value) return;

  var range  = window.getSelection().getRangeAt();
  var result = value.substring(range.startOffset,range.endOffset);
  Debug("getSelected: " + result);
  return result;
}

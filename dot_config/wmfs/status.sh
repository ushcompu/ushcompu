#!/bin/bash
 
#colors
green="#B7CE42"
grey="#666666"
white="dddddd"
dblue="#1874cd"
blue="#6F99B4"
yellow="#FEA63C"
red="#D81860"
cyan="#A7A15E"
magenta="#8B7B8B"

mpd(){
    #mpd="$(mpc | grep -)"
    mpd="$(cuchando)"
    echo "^s[right;$green;$mpd ]^s[right;$grey;ê  ]"
}

mail0(){
    mail="$(curl -u [YOUR GMAIL ACOCUNT]:[PASSWORD] --silent "https://mail.google.com/mail/feed/atom" | tr -d '\n' | awk -F '<entry>' '{for (i=2; i<=NF; i++) {print $i}}' | sed -n "s/<title>\(.*\)<\/title.*name>\(.*\)<\/name>.*/\2 - \1/p" | wc -l
)"
    echo "^s[right;$grey;mail: ]^s[right;$red;$mail  ]"
}

mail(){
    mail="$(cat ~/builds/home_ushcompu/scripts/emaiNotif/notif.data | wc -l)"
    echo "^s[right;$dblue;$mail ] ^s[right;$grey;Í  ]"
}

volume() {
  if [ "`amixer get Master | grep '\[off\]$'`" = "" ]; then
    volume=`amixer get Master | sed -n 's|.*\[\([0-9]*\)\%.*|\1%|pg' | head -n 1`
  else
    volume="off"
  fi
  echo "^s[right;$yellow;$volume ]^s[right;$grey;í  ]"
}

bat(){
#    bat="$(acpi -V | awk '{ gsub(/,/, "");} NR==1 {print $4}')"
#    echo "^s[right;$red;$bat ]^s[right;$grey;²  ]"
    battery=/sys/class/power_supply/BAT1
    if [ ! -d $battery ] ; then
      echo off
      exit 1
    fi
    
    chargeNow=$(cat $battery/charge_now)
    chargeFull=$(cat $battery/charge_full)
    chargeStatus=$(cat $battery/status)
    
    perc=$(($chargeNow * 1000 / $chargeFull))
    percRound=$((($perc+5)/10))
    
    sStatus=""
    [ "$chargeStatus" = "Charging" ] && sStatus="+"
    [ "$chargeStatus" = "Discharging" ] && sStatus="-"
    echo "^s[right;$red;${percRound}%${sStatus} ]^s[right;$grey;²  ]"
}

cpu(){
    cpu="$(eval $(awk '/^cpu /{print "previdle=" $5 "; prevtotal=" $2+$3+$4+$5 }' /proc/stat); sleep 0.4; eval $(awk '/^cpu /{print "idle=" $5 "; total=" $2+$3+$4+$5 }' /proc/stat); intervaltotal=$((total-${prevtotal:-0})); echo "$((100*( (intervaltotal) - ($idle-${previdle:-0}) ) / (intervaltotal) ))")"
    if [ $cpu -lt 10 ]; then
        cpu="0$cpu"
    fi
    echo "^s[right;$blue;$cpu% ]^s[right;$grey;Î  ]"
}

dte(){
    dte="$(date +"%a %d %b" | sed 's|á|a|;s|é|e|')"
    echo "^s[right;$cyan;$dte ]"
}
tempe(){
    tempe=$(sensors | grep '^Core 0:' | sed -re 's/.*  \+([0-9.]+?)°C.*/\1/')
    echo "^s[right;$cyan;$tempe ]^s[right;$grey;° ]"
}
tme(){
    tme="$(date +"%H:%M")"
    echo "^s[right;$blue;$tme;$clear ]"
}
messages(){
    echo "^s[right;$(mymessages show) ]"
}
 
#TIMING=1
TIMING=2
 
statustext()
{
#wmfs -c status "topbar $(mpd) $(mail) $(cpu) $(bat) $(dte) $(tme)"
    wmfs -c status "topbar $(messages) $(mpd) $(volume) $(tempe) $(cpu) $(bat) $(dte) $(tme)"
}
 
while true;
do
    statustext
    sleep $TIMING
done

#! /bin/bash

feh --bg-scale /media/Winzoz/Users/Luca/Pictures/wmfs2-darkwall.png &
urxvtd -q -f -o &
xcompmgr -C -l -O -D1 &
volumeicon &
wicd-client -t &
devilspie &
conky -c ~/.config/wmfs/scripts/conkyrc_top | while true; read line; do wmfs -c status "testbar $line"; done &
conky -c ~/.config/wmfs/scripts/conkyrc_bottom | while true; read line; do wmfs -c status "bottom $line"; done &
exec /usr/bin/wmfs

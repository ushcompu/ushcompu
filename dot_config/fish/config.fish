# Path to Oh My Fish install.
set -gx OMF_PATH "/home/matias/.local/share/omf"

# Customize Oh My Fish configuration path.
#set -gx OMF_CONFIG "/home/matias/.config/omf"

# Load oh-my-fish configuration.
source $OMF_PATH/init.fish

fish_vi_mode
set -gx EDITOR nvim
set -gx budspencer_pwdstyle long short none

alias ls="ls -lahF --color=auto"

set prefix $HOME/node_modules
set -gx NPM_PACKAGES $HOME/node_modules
set -gx PATH $HOME/bin $NPM_PACKAGES/bin $PATH
set -gx MANPATH
set -gx MANPATH $NPM_PACKAGES/share/man (manpath)

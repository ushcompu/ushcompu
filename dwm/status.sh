#!/bin/sh

mpd(){
    #mpd="$(mpc | grep -)"
    #mpd="$(cuchando)"
    curr="$(mympc_cmd current | sed -r 's/http.*&_(.*);;;.*/\1/;s/_/ /g')"
    echo -e $curr '\uf025' 
}

volume() {
  if [ "`amixer get Master | grep '\[off\]$'`" = "" ]; then
    volume=`amixer get Master | sed -n 's|.*\[\([0-9]*\)\%.*|\1%|pg' | head -n 1`
  else
    volume="off"
  fi
  echo -e $volume '\uf027' 
}

bat(){
#    bat="$(acpi -V | awk '{ gsub(/,/, "");} NR==1 {print $4}')"
#    echo "^s[right;$red;$bat ]^s[right;$grey;²  ]"
    battery=/sys/class/power_supply/BAT0
    if [ ! -d $battery ] ; then
      echo off
      exit 1
    fi
    
    chargeNow=$(cat $battery/charge_now)
    chargeFull=$(cat $battery/charge_full)
    chargeStatus=$(cat $battery/status)
    
    perc=$(($chargeNow * 1000 / $chargeFull))
    percRound=$((($perc+5)/10))
    
    sStatus=""
    [ "$chargeStatus" = "Charging" ] && sStatus="+"
    [ "$chargeStatus" = "Discharging" ] && sStatus="-"
    echo -e "${percRound}%${sStatus}" '\uf0e7'
}

cpu(){
    cpu="$(eval $(awk '/^cpu /{print "previdle=" $5 "; prevtotal=" $2+$3+$4+$5 }' /proc/stat); sleep 0.4; eval $(awk '/^cpu /{print "idle=" $5 "; total=" $2+$3+$4+$5 }' /proc/stat); intervaltotal=$((total-${prevtotal:-0})); echo "$((100*( (intervaltotal) - ($idle-${previdle:-0}) ) / (intervaltotal) ))")"
    if [ $cpu -lt 10 ]; then
        cpu="0$cpu"
    fi
    echo -e "${cpu}%" '\uf0ae'
}

dte(){
    dte="$(date +"%a %d %b" | sed 's|á|a|;s|é|e|')"
    echo $dte
}
tempe(){
    tempe=$(sensors | grep '^Core 0:' | sed -re 's/.*  \+([0-9.]+?)°C.*/\1/')
    echo "$tempe°"
}
tme(){
    tme="$(date +"%H:%M")"
    echo $tme
}
messages(){
    mymessages show
}

while true ; do
  out="$(mpd) | $(volume) | $(tempe) | $(cpu) | $(bat) | $(dte) $(tme)"
  xsetroot -name "$out"
  sleep 2
done

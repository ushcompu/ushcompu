#!/bin/sh
# totoloco at gmail dot com
# Licensed under MIT license
# www.ushcompu.com.ar
# Usage: ./pasto.sh file [type] [title] [nick] [until] [auth]
#  type: bash, text, html4strict, c, python, php, sql, ...
#  title: simply the title of your paste
#  nick: your nickname / gravatar email
#  until: paste is in the system until hours (0: forever, default: 168: 1 week)
#  auth: is an access key

base="http://pasto.elefantesrosas.com.ar"
nick=$USER
ttype="bash"
title=""
until=168
auth=""

case $# in
  1)
    text=$1
    title=$1
  ;;
  2)
    text=$1
    ttype=$2
    title=$1
  ;;
  3)
    text=$1
    ttype=$2
    title=$3
  ;;
  4)
    text=$1
    ttype=$2
    title=$3
    nick=$4
  ;;
  5)
    text=$1
    ttype=$2
    title=$3
    nick=$4
    until=$5
  ;;
  6)
    text=$1
    ttype=$2
    title=$3
    nick=$4
    until=$5
    auth=$6
  ;;
  *)
    echo "Usage: $0 file [type] [title] [nick] [until] [auth]"
    echo " type: text, html4strict, c, python, php, sql, ..."
    echo " title: simply the title of your paste"
    echo " nick: your nickname / gravatar email"
    echo " until: paste is in the system until hours (0: forever, default: 168: 1 week)"
    echo " auth: is an access key"
    exit 1
  ;;
esac
title=$(echo $title | sed 's/ /+/g')

params="data[_cli]=1&data[Pasto][type]=$ttype&data[Pasto][title]=$title&data[Pasto][nick]=$nick&data[Pasto][until]=$until&data[Pasto][auth]=$auth&data[Pasto][text]"
echo -n "$base/pastos/view/"
curl -s --data-urlencode "data[Pasto][text]@$text" -d $params "$base/pastos/add"
echo ""

exit 0

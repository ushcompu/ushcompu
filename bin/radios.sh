#!/bin/bash
#
# llama a mplayer, segun la radio indicada
#
# $Id: radio.sh,v 1.8 2007-01-04 14:48:32 javier Exp $
#
# Fixes en etapa de ejecucion por Arturo 'Buanzo' Busleiman
# <buanzo arroba buanzo.com.ar> - 20070104
# Otras radios y fixes menores por Vampii
# <vampii arroba buanzo.com.ar> - 20082304
# Mas radios por totoloco
# totoloco at gmail dot com - 20090526

host='localhost'
[ $# -ge 2 ] && host=$2
 
case "$1" in
 
    #
    # radios argentinas
    #
 
    mitre) # Radio Mitre 792 AM
        URII='mms://streammitre.uigc.net/mitrevivo'
        ;;
    rp) # Rock and Pop
        URII="rtmp://r20057.smi.vmf.edge-apps.net/magma-apps/rocknpop_radio1_high"
        ;;
    delplata) # Del Plata AM 1030
        URII='mms://delplata.telecomdatacenter.com.ar/delplata'
        ;;
    radio10) # Radio 10 (Dolina) AM 710
        URII='mms://radio10.telecomdatacenter.com.ar/radio10'
        ;;
    continental) # AM 590 Continental
        URII='http://66.175.96.10/arcontinental'
        ;;
    los40) # Los 40 Principales
        URII='http://66.175.96.10/ARLOS40P'
        ;;
    mega) # Mega 98.3 Puro Rock Nacional
        URII='http://mega.telecomdatacenter.com.ar/mega'
        ;;
    fm100) # FM 100 99.9 rtsp://g2.prima.com.ar/vivo/cadena100.rm
        URII='rtsp://g2.prima.com.ar/vivo/cadena100.rm'
        ;;
    heavy) # HEAVYMETAL.COM.AR http://65.254.42.234:15060/listen.pls
  URII="http://65.254.42.234:15060/listen.pls"
  ;;
    america) # Radio 10 (Dolina) AM 710
        URII='mms://radio10.telecomdatacenter.com.ar/radio10'
        ;;
    fmsi) # 89.1 FM BA San Isidro
        # (requiere faad/aac)
        URII='http://streaming.euro-web.com.ar:8000'
        ;;
 
    #
    # television
    #
 
    tn24) # TN 24 Horas
        URII="mms://wmedia01.uigc.net/TN"
        ;;
 
    #
    # otras radios
    #
 
    kehuelga) #Radio libre y social 102.9FM ::.K-HUELGA>
        URII="http://www.kehuelga.org:8000/radio.mp3"
        #Aca estan otros espejos en caso de saturacion:
        #http://stream.r23.cc:2323/kehuelga.mp3
        #http://radio.resistenciacreativa.org.mx:8000/radioresisteincia.mp3.m3u
        #http://radio.indymedia.org:8000/radiozapote-alta.mp3.m3u
        #http://radio.indymedia.org:8000/appo.mp3.m3u
        ;;
    bigbeat)
        URII="http://sc4.streamingpulse.net:2199/tunein/bigbeatc.plsa"
        ;;
    dnb)
        URII="http://www.bassdrive.com/v2/streams/BassDrive.pls"
        ;;
    ebm)
        URII="http://www.digitalgunfire.com/dg.pls"
        ;;
    acidjazz) # acid jazz, trip-hop, acid house, y otro electro tranqui
        URII="http://91.121.202.186/listen.pls"
        ;;
    glitch)  # RadioGlitch.com Breakbeat The Glitch Archives
        URII="http://www.radioglitch.com/stream/listen.m3u"
        ;;
    dubstep)  # DubStep.fm
        URII="http://yp.shoutcast.com/sbin/tunein-station.pls?id=7225&a=a.pls"
        ;;
    dub)  # 
        URII="http://yp.shoutcast.com/sbin/tunein-station.pls?id=58751&a=a.pls"
        ;;
    90s)  # 
        URII="http://yp.shoutcast.com/sbin/tunein-station.pls?id=1556&a=a.pls"
        ;;
    absolute)  # absolute radio
        URII="http://mp3-vr-128.smgradio.com:80/"
        ;;
    jelli)     # jelli.net
        URII="http://www.jelli.net/playlists/jelli-1.m3u"
        ;;
    folclore) # folclore argentino
        URII="http://www.impactoglobal.com:8004/"
        ;;
    radiognu) # Radio Gnu
        URII="http://radiognu.org:8000/radiognu.ogg"
        ;;
    jumanji) # radio jumanji
        URII="http://giss.tv:8000/radio_jumanji.ogg"
        ;;
    yipmania) # radio jumanji
        URII="http://giss.tv:8000/yipmania.ogg"
        ;;
    kohina) # radio chiptune
        URII="http://kohina.radio.ethz.ch:8000/kohina.ogg"
        ;;
    smoothjazz)
        URII="http://streaming.radionomy.com/all-smooth-jazz"
        ;;
    *)
        if [ $# -ge 1 ]; then
          URII=$1
        else
          echo "
  Uso: radio.sh opcion
 
        mitre       ( Radio Mitre 792 AM )
        rp          ( Rock and Pop )
        delplata    ( AM 1030 )
        los40       ( Los 40 Principales )
        fm100       ( FM 100 99.9 )
        radio10     ( AM 710 )
        continental ( AM 590 )
        mega        ( Mega 98.3 Puro Rock Nacional )
        fmsi        ( 89.1 FM BA San Isidro )
        heavy       ( Radio Heavy )
        america     ( Radio América )
 
 
        kehuelga    ( Radio libre y social 102.9FM ::.K-HUELGA )
 
        tn24        ( TN 24 Horas )

        bigbeat     ( Big Beats . ru )
        dnb         ( Bass Drive )
        ebm         ( Digital Gunfire )
        acidjazz    ( Plusfm . net )
        glitch      ( RadioGlitch . com )
        dubstep     ( DubStep.fm )
        dub         ( Dub Xtra )
        90s         ( 90s Alternative )
        absolute    ( Absolute Radio )
        jelli       ( www.jelli.net )
        folclore    ( Impacto Global )
        radiognu    ( Radio Gnu )
        jumanji     ( radio jumanji )
        kohina      ( radio chiptune )
        smoothjazz  ( All Smooth Jazz)
          "
          exit 1
        fi
        ;;
esac
 
#mplayer -af lavcresample=44100 -cache 32 "$URII"
#mplayer "$URII"

## vamos por el queridisimo mpd, primero borramos lo actual
mpc -h $host clear
# si es un pls mpd no lo parsea u.u
if [ -z $(echo $URII | grep -vi '\.pls$') ]
then
  wget -q -O - "$URII" | grep -i '^file' | sed 's/File[0-9]*=//' | mpc -h $host add
elif [ -z $(echo $URII | grep -vi '\.m3u$') ]
then
  wget -q -O - "$URII" | grep -i '^http' | mpc -h $host add
else
  mpc -h $host add "$URII"
fi
mpc -h $host play

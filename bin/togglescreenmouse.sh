#!/bin/sh

leftsize=1366
xpos=$(xdotool getmouselocation --shell | head -n1 | sed s/^X=//)

if [ $xpos -ge "$leftsize" ]
then
  left=$(($leftsize/2))
else
  left=$(($leftsize*3/2))
fi

xdotool mousemove $left 400

#!/bin/sh

out=PlayPause
if [ "$1" == "next" ] ; then
  out=Next
else
  if [ "$1" == "prev" ] ; then
    out=Previous
  fi
fi

dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 "org.mpris.MediaPlayer2.Player.$out" || spotify

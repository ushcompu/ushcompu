#!/bin/sh

dir=$HOME/.screenlayout/

option=$(dialog --stdout --backtitle "Screens" \
  --menu "Select an option:" 11 40 4 \
  1 "Only Laptop" 2 "All Screens" 3 "No VGA" 4 "No Laptop")

if [[ $? -eq 0 ]] ; then
  case $option in
    1)
      xrandr --output eDP1 --mode 1366x768 --output DP1-2 --off --output DP1-3 --off ;;
    2)
      xrandr --output eDP1 --mode 1366x768 \
        --output DP1-2 --mode 1600x900 --pos 1366x0 \
        --output DP1-3 --primary --mode 1280x1024 --pos 2966x0 ;;
    3)
      xrandr --output eDP1 --mode 1366x768 \
        --output DP1-2 --mode 1600x900 --pos 1366x0 \
        --output DP1-3 --off ;;
    4)
      xrandr --output eDP1 --off \
        --output DP1-2 --mode 1600x900 \
        --output DP1-3 --primary --mode 1280x1024 --pos 1600x0 ;;
  esac
fi

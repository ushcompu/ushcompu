#!/usr/bin/perl
 
use Irssi;
 
sub cmd_sh2cmd {
  $params = shift;
  @params = split(/\s+/, $params);
 
  if ($params[1]) {
    $cmd = $params[1];
    $out = `$cmd`;
 
    Irssi::active_win()->command("/".$params[0]." ".$out);
    return 0;
  }
}
 
Irssi::command_bind('sh2cmd', 'cmd_sh2cmd');
